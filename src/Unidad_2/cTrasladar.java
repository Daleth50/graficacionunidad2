package Unidad_2;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class cTrasladar extends JDialog
{
	JButton Ac, Ca;
	JLabel et1, et2;
	JTextField ct1, ct2;
	int dev[]=new int[2];
	
	public cTrasladar(JFrame V, boolean modal)
	{
		super(V, modal);
		setTitle("Trasladar la figura");
		setLocationRelativeTo(V);
		setSize(500, 90);
		setLayout(new FlowLayout());
		
		et1=new JLabel("Cantidad en x: ");
		et2=new JLabel("Cantidad en y: ");
		ct1=new JTextField(5);
		ct2=new JTextField(5);
		Ac=new JButton("Aceptar");
		Ca=new JButton("Cancelar");
		add(et1); add(ct1); add(et2); add(ct2); add(Ac); add(Ca);
		Ac.addActionListener(new ActionListener()
		{
			
			public void actionPerformed(ActionEvent arg0)
			{
				String v1=ct1.getText();
				String v2=ct2.getText();
				try
				{
					dev[0]=Integer.parseInt(v1);
					dev[1]=Integer.parseInt(v2);
				}
				catch(NumberFormatException e)
				{
					dev[0]=dev[1]=0;
				}
				setVisible(false);
				dispose();
			}
		});
		
		Ca.addActionListener(new ActionListener()
		{
			
			@Override
			public void actionPerformed(ActionEvent arg0)
			{
				dev=null;
				setVisible(false);
				dispose();
			}
		});
		setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
	}
	
	public int[] mostrar()
	{
		setVisible(true);
		return dev;
	}
}

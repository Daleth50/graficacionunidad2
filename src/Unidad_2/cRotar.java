package Unidad_2;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.*;

public class cRotar extends JDialog
{
	JLabel et1, et2;
	JRadioButton rb1, rb2;
	ButtonGroup gb;
	JTextField ct;
	JButton Ac, Ca;
	int dev[]=new int[2];
			
	public cRotar(JFrame f, boolean modal)
	{
		super(f, modal);
		setSize(400, 90);
		setTitle("Selecciona el sentido y elige los grados...");
		setLocationRelativeTo(f);
		setLayout(new FlowLayout(FlowLayout.LEFT));
		URL ruta = getClass().getResource("/Unidad_2/rec/rotarLeft.png");
		et1 = new JLabel(new ImageIcon(ruta));
		add(et1);
		rb1 = new JRadioButton("", true);
		add(rb1);
		ruta = getClass().getResource("/Unidad_2/rec/rotarRight.png");
		et2 = new JLabel(new ImageIcon(ruta));
		add(et2);
		rb2 = new JRadioButton();
		add(rb2);
		gb = new ButtonGroup();
		gb.add(rb1);
		gb.add(rb2);
		Ac = new JButton("Aceptar");
		Ca = new JButton("Cancelar");

		ct = new JTextField(5);

		Ac.addActionListener(new ActionListener()
		{

			public void actionPerformed(ActionEvent e)
			{
				if (rb1.isSelected())
				{
					// rotacion izq
					dev[0]=1;
				} else
				{
					// rotaci�n der
					dev[0]=2;
				}
				String grados=ct.getText();
				try
				{
					dev[1]=Integer.parseInt(grados);
				}
				catch(NumberFormatException e1)
				{
					
				}
				setVisible(false);
				dispose();
			}
		});

		Ca.addActionListener(new ActionListener()
		{

			public void actionPerformed(ActionEvent e)
			{
				dev=null;
				setVisible(false);
				dispose();
			}
		});
		add(ct);
		add(Ac);
		add(Ca);
		setVisible(true);
		setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
	}
	
	public int[] mostrar()
	{
		setVisible(true);
		return dev;
	}
}

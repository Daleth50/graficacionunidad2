package Unidad_2;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JRadioButton;

public class cRefleccion extends JDialog
{
	JLabel etiq;
	JRadioButton rbx, rby, rbxy;
	ButtonGroup gb;
	JButton Ac, Ca;
	int dev[] = new int[2];

	public cRefleccion(JFrame V, boolean modal)
	{
		super(V, modal);
		setTitle("Reflección");
		setSize(600, 90);
		setLayout(new FlowLayout());
		setLocationRelativeTo(this);
		etiq = new JLabel("Selecciona el sentido de la reflección");
		rbx = new JRadioButton("X", true);
		rby = new JRadioButton("Y");
		rbxy = new JRadioButton("XY");
		gb = new ButtonGroup();
		gb.add(rbx);
		gb.add(rby);
		gb.add(rbx);
		Ac = new JButton("Aceptar");
		Ca = new JButton("Cancelar");
		add(etiq);
		add(rbx);
		add(rby);
		add(rbxy);
		add(Ac);
		add(Ca);
		Ac.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				if (rbx.isSelected())
				{
					dev[0] = 1;
					dev[1] = -1;
				} else
				{
					if (rby.isSelected())
					{
						dev[0] = -1;
						dev[1] = 1;
					} else
						dev[0] = -1;
					dev[1] = -1;
				}
				setVisible(false);
				dispose();
			}
		});

		Ca.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				dev = null;
				setVisible(false);
				dispose();
			}
		});
		setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
	}

	public int[] mostrar()
	{
		setVisible(true);
		return dev;
	}
}

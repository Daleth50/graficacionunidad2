package Unidad_2;

import java.awt.Color;
import java.awt.Graphics;

class Punto
{
	float x, y;

	public Punto(float a, float b)
	{
		x = a;
		y = b;
	}
}

public class Figura
{
	Punto fig[];
	Punto nueva[];
	Punto Original[];
	int fig1[] = { 300, 300, 300, 400, 400, 400, 400, 300, 350, 200, 300, 300 };

	int fig2[][] = { { 300, 300 }, { 300, 400 }, { 400, 400 }, { 400, 300 }, { 350, 200 }, { 300, 300 } };

	Punto fig3[] = { new Punto(350, 300), new Punto(300, 300), new Punto(300, 400), new Punto(400, 400),
			new Punto(400, 300), new Punto(350, 200), new Punto(300, 300) };

	public Figura(Punto f[])
	{
		fig = f;
		nueva = new Punto[fig.length];
		Original = new Punto[f.length];
		for (int i = 0; i < nueva.length; i++)
		{
			nueva[i] = new Punto(0f, 0f);
			Original[i]=new Punto(f[i].x, f[i].y);
		}
	}

	public void restaurar()
	{
		for (int i = 0; i < Original.length; i++)
		{
			fig3[i].x = Original[i].x;
			fig3[i].y = Original[i].y;
		}
	}

	public void dibujar1(Graphics g)
	{
		g.setColor(Color.BLUE);
		for (int i = 0, j = 1; i < fig1.length - 2; j += 2, i += 2)
			g.drawLine(fig1[i], fig1[j], fig1[i + 2], fig1[j + 2]);
	}

	public void dibujar2(Graphics g)
	{
		g.setColor(Color.BLUE);
		for (int i = 0; i < fig2.length - 1; i += 1)
			g.drawLine((int) fig2[i][0], (int) fig2[i][1], (int) fig2[i + 1][0], (int) fig2[i + 1][1]);
	}

	public void dibujar(Graphics g)
	{
		g.setColor(Color.BLUE);
		for (int i = 1; i < fig3.length - 1; i += 1)
			g.drawLine((int) fig3[i].x, (int) fig3[i].y, (int) fig3[i + 1].x, (int) fig3[i + 1].y);
		g.setColor(Color.CYAN);
		for (int i = 1; i < fig3.length - 1; i += 1)
			g.drawLine((int) nueva[i].x, (int) nueva[i].y, (int) nueva[i + 1].x, (int) nueva[i + 1].y);
	}

	public void escalarO(double escA, double escB)
	{// xA, yB
		for (int i = 0; i < fig3.length; i++)
		{
			fig3[i].x *= escA;
			fig3[i].y *= escB;
		}
	}

	public void escalarP(double escA, double escB)
	{
		int Tx = (int) fig3[0].x, Ty = (int) fig3[0].y;
		trasladar((int) -Tx, (int) -Ty);
		escalarO(escA, escB);
		trasladar(Tx, Ty);
	}

	public void deformarO(float dA, float dB)
	{// xA, yB
		for (int i = 0; i < fig3.length; i++)
		{
			double x = fig[i].x;
			fig3[i].x = (int) (fig3[i].x + fig3[i].y * dA);
			fig3[i].y = (int) (fig3[i].x * dB + fig3[i].y);
		}
	}

	public void rotarP(int grados)
	{
		int Tx = (int) fig3[0].x, Ty = (int) fig3[0].y;
		trasladar((int) -Tx, (int) -Ty);
		rotarC(grados);
		trasladar(Tx, Ty);
	}

	public void deformarP(float dA, float dB)
	{
		int Tx = (int) fig3[0].x, Ty = (int) fig3[0].y;
		trasladar((int) -Tx, (int) -Ty);
		deformarO(dA, dB);
		trasladar(Tx, Ty);
	}

	public void rotarC(int grados)
	{
		float rad = (float) Math.toRadians(grados);
		float sena = (float) Math.sin(rad);
		float cosa = (float) Math.cos(rad);
		for (int i = 0; i < fig3.length; i++)
		{
			double x = fig3[i].x;
			fig3[i].x = (float) (x * cosa - fig3[i].y * sena);
			fig3[i].y = (float) (x * sena + fig3[i].y * cosa);
		}
	}

	public void refleccionP(int r1, int r2)
	{
		int Tx = (int) fig3[0].x, Ty = (int) fig3[0].y;
		trasladar((int) -Tx, (int) -Ty);
		escalarO(r1, r2);
		trasladar(Tx, Ty);
	}

	public boolean verificar(int x, int y)
	{
		double minxf = 0, maxxf = 0, minyf = 0, maxyf = 0;
		minxf = fig3[0].x;
		minyf = fig3[0].y;
		for (int i = 1; i < fig3.length; i++)
		{
			if (fig3[i].x < minxf)
				minxf = fig3[i].x;
			if (fig3[i].y < minyf)
				minyf = fig3[i].y;
			if (fig3[i].x > maxxf)
				maxxf = fig3[i].x;
			if (fig3[i].y > maxyf)
				maxyf = fig3[i].y;
		}

		if (x >= minxf && x <= maxxf && y >= minyf && y <= maxxf)
			return true;
		return false;
	}

	public void trasladar(int Tx, int Ty)
	{
		for (int i = 0; i < fig3.length; i++)
		{
			fig3[i].x += Tx;
			fig3[i].y += Ty;
		}
	}

	public void rotarH(int grados)
	{
		float rad = (float) Math.toRadians(grados);
		float seno = (float) Math.sin(rad);
		float coseno = (float) Math.cos(rad);
		int Tx = (int) fig[0].x;
		int Ty = (int) fig[0].y;
		for (int i = 0; i < fig3.length; i++)
		{
			double x = fig3[i].x;
			double y = fig3[i].y;
			fig3[i].x = (float) (x * coseno + y * seno - Tx * coseno - Ty * seno + Tx);
			fig3[i].y = (float) (-x * seno + y * coseno + Tx * seno - Ty * coseno + Ty);
		}
	}

	public void escalarH(float escX, float escY)
	{
		int Tx = (int) fig[0].x;
		int Ty = (int) fig[0].y;
		for (int i = 0; i < fig3.length; i++)
		{
			double x = fig3[i].x;
			double y = fig3[i].y;
			fig3[i].x = (float) (escX * x - Tx * escX + Tx);
			fig3[i].y = (float) (escY * y - Ty * escY + Ty);
		}
	}

	public void refleccionH(float rX, float rY)
	{
		int Tx = (int) fig3[0].x;
		int Ty = (int) fig3[0].y;
		for (int i = 0; i < fig3.length; i++)
		{
			double x = fig3[i].x;
			double y = fig3[i].y;
			fig3[i].x = (float) (rX * x - Tx * rX + Tx);
			fig3[i].y = (float) (rY * y - Ty * rY + Ty);
		}
	}

	public void mapeo(int xwMax, int ywMax)
	{
		float sx = ((float) (xwMax - 600.0) / (float) (xwMax));
		float sy = ((float) (ywMax - 450.0) / (float) (ywMax));
		for (int i = 0; i < fig3.length; i++)
		{
			nueva[i].x = fig3[i].x * sx + 600;
			nueva[i].y = fig3[i].y * sy + 450;
		}
	}
	
	public void rotarHs(int grados)
	{
		float rad = (float) Math.toRadians(grados);
		float seno = (float) Math.sin(rad);
		float coseno = (float) Math.cos(rad);
		int Tx = (int) fig[0].x;
		int Ty = (int) fig[0].y;
		for (int i = 0; i < fig3.length; i++)
		{
			double x = fig3[i].x;
			double y = fig3[i].y;
			fig3[i].x = (float) (x * coseno - y * seno - Tx * coseno + Ty * seno + Tx);
			fig3[i].y = (float) (x * seno + y * coseno - Tx * seno - Ty * coseno + Ty);
		}
	}
	
	public void deformarH(float cd)
	{
		float Tx = fig3[0].x;
		float Ty = fig3[0].y;
		for (int i = 0; i < fig3.length; i++)
		{
			float x = fig3[i].x;
			float y = fig3[i].y;
			fig3[i].x = x+cd*y-cd*Ty;
			fig3[i].y = cd*x+y-cd*Tx;
		}
	}
}

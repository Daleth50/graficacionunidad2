package Unidad_2;



import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.net.URL;

import javax.swing.*;

public class Interfaz extends JPanel implements /*ActionListener,*/ MouseWheelListener
{
	JFrame Vent;
	Figura F;
	JButton b1, b2, b3, b4, b5;
	boolean mover;
	JMenuBar barraM;
	JMenu op1, op2;
	JMenuItem m1, m2, m3, m4, m5, m6, m7;
	JToolBar barraH;
	Action a1, a2, a3, a4;
	Image fondo;

	public void ponerMenu()
	{
		barraM = new JMenuBar();
		Vent.setJMenuBar(barraM);
		op1 = new JMenu("Transformaciones");
		op2 = new JMenu("Acerca de...");
		barraM.add(op1);
		barraM.add(op2);

		m1 = new JMenuItem("Restaurar");
		m1.setMnemonic('R');
		m1.setToolTipText("Restaura a la figura original");
		m1.setAccelerator(KeyStroke.getKeyStroke('R', InputEvent.CTRL_MASK));
		URL ruta = getClass().getResource("/Unidad_2/Rec/rest.png");
		;
		m1.setIcon(new ImageIcon(ruta));
		m1.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				F.restaurar();
				repaint();
			}
		});
		m2 = new JMenuItem("Escalar");
		m2.setMnemonic('E');
		m2.setToolTipText("Escala la figura la cantidad especificada");
		m2.setAccelerator(KeyStroke.getKeyStroke('E', InputEvent.CTRL_MASK));
		ruta = getClass().getResource("/Unidad_2/Rec/esca.png");
		;
		m2.setIcon(new ImageIcon(ruta));
		m2.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				cEscalar obj = new cEscalar(Vent, true, "Define la cantidad a escalar", true);
				float esc = obj.mostrar();
				if (esc != -1)
					F.escalarP(esc, esc);
				repaint();
			}
		});

		m3 = new JMenuItem("Rotar");
		m3.setMnemonic('o');
		m3.setToolTipText("Permite rotarla figura los grados que se especifiquen");
		m3.setAccelerator(KeyStroke.getKeyStroke('O', InputEvent.CTRL_MASK));
		ruta = getClass().getResource("/Unidad_2/Rec/rotar.png");
		;
		m3.setIcon(new ImageIcon(ruta));
		m3.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				cRotar obj = new cRotar(Vent, true);
				int v[] = obj.mostrar();
				if (v != null && v[1] != 0)
				{
					if (v[0] == 1)
					{
						// Rotar en contra
						F.rotarH(v[1]);
					} else
					{
						// Rotar en el sentido de las manecillas
						F.rotarHs(v[1]);
					}
					repaint();
				}
			}
		});
		m4 = new JMenuItem("Deformar");
		m4.setMnemonic('D');
		m4.setToolTipText("Deforma la figura, valor m�ximo 1");
		m4.setAccelerator(KeyStroke.getKeyStroke('D', InputEvent.CTRL_MASK));
		ruta = getClass().getResource("/Unidad_2/Rec/def.png");
		m4.setIcon(new ImageIcon(ruta));
		m4.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg0)
			{
				cEscalar obj = new cEscalar(Vent, true, "Dame la cantidad a deformar", false);
				float res=obj.mostrar();
				if(res!=-1)
				{
					F.deformarH(res);
					repaint();
				}
			}
		});
		
		m5=new JMenuItem("Reflecci�n");
		m5.setMnemonic('F');
		m5.setToolTipText("Realiza el proceso de reflecci�n en los diferentes");
		m5.setAccelerator(KeyStroke.getKeyStroke('F', InputEvent.CTRL_MASK));
		ruta = getClass().getResource("/Unidad_2/Rec/reflejarg.png");
		m5.setIcon(new ImageIcon(ruta));
		m5.addActionListener(new ActionListener()
		{
			
			public void actionPerformed(ActionEvent e)
			{
				cRefleccion obj=new cRefleccion(Vent, true);
				int res[]=obj.mostrar();
				if(res!=null)
				{
					F.escalarH(res[0], res[1]);
					repaint();
				}
			}
		});
		
		m6=new JMenuItem("Trasladar");
		m6.setMnemonic('T');
		m6.setToolTipText("Permite mover la cantidad que se especifique");
		m6.setAccelerator(KeyStroke.getKeyStroke('T', InputEvent.CTRL_MASK));
		ruta = getClass().getResource("/Unidad_2/Rec/mov.png");
		m6.setIcon(new ImageIcon(ruta));
		m6.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				cTrasladar obj=new cTrasladar(Vent, true);
				int res[]=obj.mostrar();
				if(res!=null)
				{
					F.trasladar(res[0], res[1]);
					repaint();
				}
			}
		});
		
		m7=new JMenuItem("Salir");
		m7.setMnemonic('S');
		m7.setAccelerator(KeyStroke.getKeyStroke('S', InputEvent.CTRL_MASK));
		ruta = getClass().getResource("/Unidad_2/Rec/s.png");
		m7.setIcon(new ImageIcon(ruta));
		m7.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg0)
			{
				System.exit(0);
				
			}
		});
		
		op1.add(m1);
		op1.add(m2);
		op1.add(m3);
		op1.add(m4);
		op1.add(m5);
		op1.add(m6);
		op1.addSeparator();
		op1.add(m7);
	}

	public Interfaz(Punto f[])
	{
		Vent = new JFrame("Transformaciones en 2D");
		Vent.setSize(800, 600);
		ponerMenu();
		F = new Figura(f);
		setSize(800, 600);
		Vent.add(this, BorderLayout.CENTER);
		//Barra de Herramientas
		barraH=new JToolBar("Operaciones frecuentes", JToolBar.VERTICAL);
		Vent.add(barraH, BorderLayout.EAST);
		URL ruta=getClass().getResource("/Unidad_2/Rec/rest.png");
		a1=new AbstractAction("", new ImageIcon(ruta))
		{
			public void actionPerformed(ActionEvent e)
			{
				F.restaurar();
				repaint();
			}
		};
		
		a1.putValue(Action.SHORT_DESCRIPTION, "Restaurar la figura a las coordenada originales");

		b1 = new JButton(a1);
		barraH.add(b1);
		barraH.setFloatable(false);
		ruta=getClass().getResource("/Unidad_2/Rec/rotarLeft.png");
		a2=new AbstractAction("", new ImageIcon(ruta))
		{
			public void actionPerformed(ActionEvent e)
			{
				F.rotarH(5);
				repaint();
			}
		};
		a2.putValue(Action.SHORT_DESCRIPTION, "Rota la figura a la izquierda 5 grados");
		b2 = new JButton(a2);
		barraH.add(b2);
		ruta=getClass().getResource("/Unidad_2/Rec/rotarRight.png");
		a3=new AbstractAction("", new ImageIcon(ruta))
		{
			public void actionPerformed(ActionEvent e)
			{
				F.rotarH(-5);
				repaint();
			}
		};
		a3.putValue(Action.SHORT_DESCRIPTION, "Rota la figura a la derecha 5 grados");
		b3 = new JButton(a3);
		barraH.add(b3);
		ruta=getClass().getResource("/Unidad_2/Rec/s.png");
		a4=new AbstractAction("", new ImageIcon(ruta))
		{
			public void actionPerformed(ActionEvent e)
			{
				System.exit(0);
			}
		};
		a4.putValue(Action.SHORT_DESCRIPTION, "Salir");
		b4 = new JButton(a4);
		barraH.add(b4);
		b5 = new JButton("Reflecci�n");
		/*JPanel Abajo = new JPanel(new GridLayout(1, 5));
		Abajo.add(b1);
		Abajo.add(b2);
		Abajo.add(b3);
		Abajo.add(b4);
		Abajo.add(b5);
		Vent.add(Abajo, BorderLayout.SOUTH);*/

		/*b1.addActionListener(this);
		b2.addActionListener(this);
		b3.addActionListener(this);
		b4.addActionListener(this);
		b5.addActionListener(this);*/
		addMouseListener(new MouseAdapter()
		{
			public void mousePressed(MouseEvent e)
			{
				// Detectar que yo le de clic dentro de la figura
				int cx = e.getX();
				int cy = e.getY();
				mover = F.verificar(cx, cy);
				System.out.println(mover);
			}
		});

		addMouseMotionListener(new MouseMotionAdapter()
		{
			public void mouseDragged(MouseEvent e)
			{
				int cx = e.getX();
				int cy = e.getY();
				if (mover)
				{
					double Tx = cx - F.fig3[0].x;
					double Ty = cy - F.fig3[0].y;
					F.trasladar((int) Tx, (int) Ty);
					repaint();
				}
			}
		});

		addMouseWheelListener(this);
		
		ruta=getClass().getResource("/Unidad_2/Rec/fonTrans.jpg");
		fondo=new ImageIcon(ruta).getImage();

		Vent.setVisible(true);
		Vent.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
	}

	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		g.drawImage(fondo, 0, 0, getSize().width, getSize().height, this);
		g.fillRect(600, 450, 200, 150);
		F.mapeo(getSize().width, getSize().height);
		F.dibujar(g);
	}

	public static void main(String[] args)
	{
		Punto fig3[] = { new Punto(350, 300), new Punto(300, 300), new Punto(300, 400), new Punto(400, 400),
				new Punto(400, 300), new Punto(350, 200), new Punto(300, 300) };
		new Interfaz(fig3);
	}

	/*public void actionPerformed(ActionEvent e)
	{
		if (e.getSource() == b1)
		{
			String A = JOptionPane.showInputDialog("Dame el factor de escalamiento x");
			String B = JOptionPane.showInputDialog("Dame el factor de escalamiento y");
			F.escalarH(Float.parseFloat(A), Float.parseFloat(B));
		} else
		{
			if (e.getSource() == b2)
			{
				String A = JOptionPane.showInputDialog("Dame el factor de escalamiento x");
				String B = JOptionPane.showInputDialog("Dame el factor de escalamiento y");
				F.deformarO(Float.parseFloat(A), Float.parseFloat(B));
			} else if (e.getSource() == b3)
			{
				String g = JOptionPane.showInputDialog("Dame los grados a rotar");
				int gr = Integer.parseInt(g);
				F.rotarH(gr);
			} else if (e.getSource() == b4)
			{
				String A = JOptionPane.showInputDialog("Cantidad a trasladar en x");
				String B = JOptionPane.showInputDialog("Cantidad a trasladar en y");
				F.trasladar(Integer.parseInt(A), Integer.parseInt(B));
			} else if (e.getSource() == b5)
			{
				String A = JOptionPane.showInputDialog("Si quieres reflexion en x escribe -1 si no 1");
				String B = JOptionPane.showInputDialog("Si quieres reflexi�n en y escribe -1 si no 1");
				F.refleccionH(Float.parseFloat(A), Float.parseFloat(B));
			}
		}
		repaint();
	}*/

	public void mouseWheelMoved(MouseWheelEvent e)
	{
		int rotacion = e.getWheelRotation();
		if (rotacion < 0)
		{// Arriba
			F.escalarP(1.05, 1.05);
		} else
		{// Abajo
			F.escalarP(.98, .98);
		}
		repaint();
	}
}
